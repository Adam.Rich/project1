#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "matrix.h"

static bool is_decimal(char ch)
{
	return ((ch <= '9') && (ch >= '0'));
}


matrix_t matrix_create(const char* matrix_string)
{
        matrix_t matrix;
        if(!matrix_string){
            return matrix_empty;
        } 
        int i = 0;
        int j;
        int row_counter = 1;
        float space_counter = 0;
        float incorrect_char = 0;
        int matrix_counter = 0;
        const int empty = '\0';
        int elementcounter = 0;
        char element[20];

        for(j = 0;j < (int)strlen(matrix_string);j++){
            char chr = matrix_string[j];
            if(chr == ' '){
                matrix.elements[matrix_counter] = atoi(element);
                matrix_counter += 1;
                elementcounter = 0;
                space_counter += 1;
                for(i = 0; i < 20 ; i++){
                    element[i] = empty;
                }
            }          
            if(chr == '\n'){
                row_counter += 1;
                matrix.elements[matrix_counter] = atoi(element);
                matrix_counter += 1;
                elementcounter = 0;
                for(i = 0; i < 20 ; i++){
                    element[i] = empty;
                }
            }
            if(is_decimal(chr) || chr == '-'){
                element[elementcounter] = chr;
                elementcounter += 1;
            }
            if(!is_decimal(chr) && chr != ' ' &&
                chr != '\n' && chr != '-'){
                incorrect_char = 1;
                break;
            }
        }
        if(j == (int)strlen(matrix_string)){
                matrix.elements[matrix_counter] = atoi(element);
                for(i = 0; i < (int)strlen(element) ; i++){
                    element[i] = '\0';
                }
        }
        int check = space_counter/row_counter;
        float check2 = space_counter/row_counter;
	if(check != check2 || incorrect_char){
		return matrix_empty;
        }else {
                matrix.width = (space_counter/row_counter)+1;
                matrix.height = row_counter;
                return matrix;
        }
}


void matrix_print(const matrix_t* matrix)
{
        int i,j;
        for(i = 0; i < (int)matrix->height;i++){
            for(j = (int)(matrix->width*i) ;j < (int)((i+1)*matrix->width) ;j++){
                if(j == (int)((matrix->width*(i+1))-1)){
                    printf("%d",matrix->elements[j]);
                }else{
                    printf("%d, ", matrix->elements[j]);
                }
            }
            printf("\n");
        }         
}


bool matrix_equal(const matrix_t* a, const matrix_t* b)
{
        int j = 0;
        int equal = 0;
        if(a->height == b->height && a->width == b->width){
            for(int i = 0; i < (int)(a->height*a->width);i++){
                if(a->elements[i] != b->elements[i]){      
                    j++;  
                    break;
                }
            }
        }
        if(j == (int)(a->height*a->width)){
            equal = 1;
        }
        return equal;
}


size_t matrix_row(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len)
{
        int counter  = 0;
	if (!matrix || (index >= matrix->height)
	    || !values || (len < matrix->width)){
		return 0;
        } else{
            for(int i = (matrix->width*index); i < (int)(matrix->width*(index+1)); i++){
                values[counter]=matrix->elements[i];
                counter += 1;
            }
	    return matrix->width;
        }
}


size_t matrix_col(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len)
{
	if (!matrix || (index >= matrix->width)
	    || !values || (len < matrix->height)){
		return 0;
	}else {
            int counter = 0;
            for(int i = 0; i < (int)matrix->height;i++){
                values[counter] = matrix->elements[index +(matrix->width*i)];
                counter += 1;            
            }
            return matrix->height;
        }
}




